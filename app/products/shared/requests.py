from app.authentification.shared.request import ValidRequest, InvalidRequest
from app.authentification.shared.validation import ValidationService, RequiredRule, BooleanRule, FloatRule, IntegerRule, DateTimeRule


class AddProductRequest(ValidRequest):
    @classmethod
    def build_from_dict(cls, adict, files=None):
        invalid_request = InvalidRequest()
        if not isinstance(adict, dict):
            invalid_request.add_error('No Field', 'Not correct email')
            return invalid_request
        validation_service = ValidationService(adict)
        validation_service.add_rules({
            'name': [RequiredRule.build()],
            'description': [RequiredRule.build()],
            'availability': [RequiredRule.build(),
                             BooleanRule.build()],
            'quality': [RequiredRule.build()],
            'price': [RequiredRule.build(),
                      FloatRule.build()],
            'stock': [RequiredRule.build(),
                      IntegerRule.build()],
            'manufacturer': [RequiredRule.build()],
            'categories': [RequiredRule.build()],
        })
        file_validation = ValidationService(files)
        file_validation.add_rules({
            'images': [RequiredRule.build()],
        })
        for x in ('ref_part', 'weight', 'diameter', 'dimension', 'date_of_prod', \
                'num_oem', 'country_of_origin', 'volume_of_part'): 
            if x in adict:
                validation_service.add_rules({
                    'ref_part': [RequiredRule.build()],
                    'weight': [RequiredRule.build(), IntegerRule.build()],
                    'diameter': [RequiredRule.build(),
                                 IntegerRule.build()],
                    'dimension': [RequiredRule.build()],
                    'date_of_prod': [RequiredRule.build(),
                                     DateTimeRule.build()],
                    'num_oem': [RequiredRule.build()],
                    'country_of_origin': [RequiredRule.build()],
                    'volume_of_part': [RequiredRule.build(),
                                       IntegerRule.build()],
                })
                break

        result = {**validation_service.validate(), **file_validation.validate()}
        invalid_request = InvalidRequest.from_dict(result)

        if invalid_request.has_errors():
            return invalid_request

        new_dict = {**adict, "files": files, "availability": int(adict["availability"])}
        return AddProductRequest(new_dict)

class ListProductsRequest(ValidRequest):
    @classmethod
    def build_from_dict(cls, adict):
        return cls(adict)

