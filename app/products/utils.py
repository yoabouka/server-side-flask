from app.products.serializer import ProductListSerializer


def to_json_serializable(lists):
    list_products = []
    if lists:
        for p in lists:
            list_products.append(ProductListSerializer(p).data)

    return list_products
