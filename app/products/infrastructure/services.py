import json
from ..domain.entities import Product, PartData, Image
from flask_uploads import IMAGES, UploadSet
from app.categories.infrastructure.services import CategoryEncoder
from app.factory import photos


class ProductEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Product):
            return {
                "id":
                obj.id,
                "name":
                obj.name,
                "description":
                obj.description,
                "availability":
                obj.availability,
                "quality":
                obj.quality,
                "price":
                obj.price,
                "stock":
                obj.stock,
                "manufacturer":
                obj.manufacturer,
                "part_data":
                json.loads(json.dumps(obj.part_data, cls=PartDataEncode)),
                "categories":
                json.loads(json.dumps(obj.categories, cls=CategoryEncoder)),
                "images":
                json.loads(json.dumps(obj.images, cls=ImageEncoder))
            }

        return json.JSONEncoder.default(self, obj)


class PartDataEncode(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, PartData):
            return {
                "ref_part": obj.ref_part,
                "weight": obj.weight,
                "diameter": obj.diameter,
                "dimension": obj.dimension,
                "date_of_prod": obj.date_of_prod,
                "num_oem": obj.num_oem,
                "country_of_origin": obj.country_of_origin,
                "volume_of_part": obj.volume_of_part,
            }
        return json.JSONEncoder.default(self, obj)

class ImageEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Image):
            return {
                "url": photos.url(obj.file_name)
            }
        return json.JSONEncoder.default(self, obj)

class FlaskFileService:
    def save_batch(self, files):
        images = []
        for f in files.values():
            filename = photos.save(f)
            blob = f.read()
            rec = Image.from_dict({
                "file_name": filename,
                "file_path": photos.path(filename),
                "file_size": len(blob),
                "original_name": f.filename
                })
            images.append(rec)
        return images
