from ..domain.repository import ProductRepository, ImageRepository
from flask import request
from app.file_Uploads.models import ProductImage
from ..models import Product, db, PartData
import app.products.domain.entities as ent
from datetime import datetime


class FlaskProductRepository(ProductRepository):
    def __init__(self, category_repo):
        self.category_repo = category_repo

    def save(self, product, part_data, categories, images):
        part_data_to_save = None
        if product.part_data:
            part_data_to_save = PartData(
                ref_part=product.part_data.ref_part,
                weight=product.part_data.weight,
                diameter=product.part_data.diameter,
                dimension=product.part_data.dimension,
                date_of_prod=datetime.strptime(product.part_data.date_of_prod,
                                               "%Y-%m-%d %H:%M:%S"),
                num_oem=product.part_data.num_oem,
                country_of_origin=product.part_data.country_of_origin,
                volume_of_part=product.part_data.volume_of_part,
            )
        product_to_save = Product(name=product.name,
                                  description=product.description,
                                  availability=product.availability,
                                  quality=product.quality,
                                  price=product.price,
                                  stock=product.stock,
                                  images=product.images,
                                  seller_id=product.seller,
                                  manufacturer=product.manufacturer,
                                  partdata=part_data_to_save)
        product_to_save.categories = self.category_repo.get_in_list(
            product.categories)

        db.session.add(product_to_save)
        db.session.commit()
        product.id = product_to_save.id
        print(product.images)
        product.categories = product_to_save.categories
        return product

    def paginate(self):
        products = Product.query.paginate()
        return {
                "items": [ent.Product(name=product.name,
                                  description=product.description,
                                  availability=product.availability,
                                  quality=product.quality,
                                  price=product.price,
                                  stock=product.stock,
                                  seller_id=product.seller,
                                  manufacturer=product.manufacturer,
                                  images=[
                                      ent.Image(file_name=i.file_name,
                                          file_path=i.file_path,
                                          file_size=i.file_size,
                                          original_name=i.original_name)
                                      for i in product.images],
                                  partdata=product.part_data_to_save)
                                  for product in products.items
                    ],
                'page_meta': {
                    'total_items_count' : products.total,
                    'current_page_number' : products.page,
                    'prev_page_number' : products.prev_num or 1,
                    'total_pages_count' : products.pages,
                    'has_next_page' : products.has_next,
                    'has_prev_page' : products.has_prev,
                    'next_page_number' : products.next_num or None
                }
            }



class FlaskImageRepository(ImageRepository):
    def save_batch(self, images):
        new_images = list(map(lambda i: ProductImage(file_name=i.file_name,
            file_path=i.file_path, file_size=i.file_size,
            original_name=i.original_name), images))
        db.session.add_all(new_images)
        db.session.commit()
        return images;

