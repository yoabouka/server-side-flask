from .entities import Product
from app.authentification.shared.use_case import UseCase
import app.authentification.shared.response as res


class AddProductUseCase(UseCase):
    def __init__(self, product_repo, file_service, image_repo):
        self.product_repo = product_repo
        self.image_repo = image_repo
        self.file_service = file_service

    def process_request(self, request):
        images = self.file_service.save_batch(request.files)
        images = self.image_repo.save_batch(images)
        data = {**request.attributes, "images": images}
        product = Product.from_dict(data)
        saved_product = self.product_repo.save(product)
        return res.ResponseSuccess(saved_product)

class ListProductsUseCase(UseCase):
    def __init__(self, product_repo):
        self.product_repo = product_repo

    def process_request(self, request):
        products = self.product_repo.paginate(request.attributes)
        return res.ResponseSuccess(products)
