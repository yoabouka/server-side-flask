class ProductRepository:
    def save(self, product):
        raise NotImplementedError

    def paginate(self):
        raise NotImplementedError

class ImageRepository:
    def save_batch(self, image):
        raise NotImplementedError
