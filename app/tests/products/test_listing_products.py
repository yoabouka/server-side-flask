import json
import random
from faker import Faker
from app.products.models import Product
from app.categories.models import Category
from .. import AppTestCase

class ListProductsTest(AppTestCase):
    def product_seeder(self, seller_id):
        fake = Faker()
        quality = ['origine', 'adaptable']
        products = []
        for i in range(5):
            product = Product(name=fake.sentence(), slug=fake.slug(),
                          description=fake.text(max_nb_chars=200), availability=True, quality=random.choice(quality),
                          price=fake.random_int(min=50, max=2500), stock=fake.random_int(min=5, max=500),
                          rating=fake.random_int(min=0, max=5),
                          seller_id=seller_id,
                          manufacturer=fake.sentence(),
                          publish_on=fake.date_time())
            products.append(product)
        return products

    def test_list_products_works_fine(self):
        self.authenticate("ROLE_SELLER")
        products = self.product_seeder(1)
        products_name = list(map(lambda x: x.name, products))
        with self.app.app_context():
            self.db.session.add_all(products)
            self.db.session.commit()
        result = self.client().get("/products/")
        self.assertEqual(200, result.status_code)
        json_result = json.loads(result.data)
        self.assertIn("items", json_result)
        self.assertEqual(5, len(json_result.get('items')))
        self.assertCountEqual(
            products_name,
            list(map(lambda x: x.get('name'), json_result.get('items'))))
