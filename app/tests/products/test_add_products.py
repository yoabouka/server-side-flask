from .. import AppTestCase
import io
from app.products.models import Product
import uuid
from faker import Faker
from app.categories.models import Category
import json


class AddProductTest(AppTestCase):
    def product_data(self):
        fake = Faker()
        with self.app.app_context():
            self.db.session.add(Category(name=fake.name()))
            self.db.session.commit()

        return {
            "name": fake.name(),
            "description": fake.text(),
            "availability": 1,
            "images": [(io.BytesIO(b"abcdef"), 'test.jpg')],
            "quality": fake.word(),
            "price": fake.pyfloat(min_value=20.00, max_value=10000.00),
            "stock": fake.pyint(),
            "manufacturer": fake.name(),
            "categories": [1]
        }

    def part_data_data(self):
        fake = Faker()
        return {
            "ref_part": fake.word(),
            "weight": fake.pyint(),
            "diameter": fake.pyint(),
            "dimension": fake.word(),
            "date_of_prod": fake.date_time().strftime("%Y-%m-%d %H:%M:%S"),
            "num_oem": str(uuid.uuid4()),
            "country_of_origin": fake.country(),
            "volume_of_part": fake.pyint()
        }

    def test_create_product_as_seller_works(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(200, response.status_code)
        with self.app.app_context():
            self.assertEqual(1, Product.query.count())
            product = Product.query.get(1)
        self.assertIn("id", json.loads(response.data))

    def test_create_a_product_with_part_data_as_seller_works(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data.update(self.part_data_data())
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(200, response.status_code)
        with self.app.app_context():
            self.assertEqual(1, Product.query.count())
            product = Product.query.get(1)
        self.assertIn("id", json.loads(response.data))

    def test_send_a_product_with_non_valid_attributes_fails(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        for key in data:
            data = self.product_data()
            data.pop(key, None)
            response = self.client().post(
                "/products/",
                headers=dict(Authorization=f"Bearer {access_token}"),
                data=data,
                content_type="multipart/form-data")
            self.assertEqual(422, response.status_code)
            self.assertIn(key, json.loads(response.data).get("message"))
            with self.app.app_context():
                self.assertEqual(0, Product.query.count())

    def test_sending_another_value_than_float_for_price_fails(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data['price'] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn("price", json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())

    def test_sending_another_value_than_int_for_stock_fails(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data['stock'] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn("stock", json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())

    def test_date_of_prod_is_a_datetime(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data.update(self.part_data_data())
        data["date_of_prod"] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn("date_of_prod", json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())
            product = Product.query.get(0)

    def test_weight_value_than_int_should_fails(
            self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data.update(self.part_data_data())
        key = 'weight'
        data[key] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn(key, json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())
            product = Product.query.get(0)

    def test_diameter_value_than_int_should_fails(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        key = 'diameter'
        data[key] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn(key, json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())
            product = Product.query.get(0)

    def test_volume_of_part_value_than_int_should_fails(
            self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data.update(self.part_data_data())
        key = 'volume_of_part'
        data[key] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn(key, json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())
            product = Product.query.get(0)

    def test_date_of_prod_is_a_datetime(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        data.update(self.part_data_data())
        data["date_of_prod"] = "false data"
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(422, response.status_code)
        self.assertIn("date_of_prod", json.loads(response.data).get("message"))
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())
            product = Product.query.get(0)

    def test_at_least_one_image_is_required_for_a_product(self):
        access_token = self.authenticate("ROLE_SELLER")
        data = self.product_data()
        response = self.client().post(
                "/products/", data=data,
                content_type='multipart/form-data',
                headers=dict(Authorization=f"Bearer {access_token}"))
        self.assertEqual(200, response.status_code)
        with self.app.app_context():
            self.assertEqual(1, Product.query.count())
            product = Product.query.get(1)
            self.assertEqual(1, len(product.images))

    def test_create_a_product_as_user_fails(self):
        access_token = self.authenticate()
        data = self.product_data()
        data.update(self.part_data_data())
        response = self.client().post(
            "/products/",
            headers=dict(Authorization=f"Bearer {access_token}"),
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(401, response.status_code)
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())

    def test_create_a_product_as_a_guest_fails(self):
        data = self.product_data()
        data.update(self.part_data_data())
        response = self.client().post(
            "/products/",
            data=data,
            content_type="multipart/form-data")
        self.assertEqual(401, response.status_code)
        with self.app.app_context():
            self.assertEqual(0, Product.query.count())
