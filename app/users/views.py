from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_refresh_token, create_access_token

from app.factory import db, bcrypt
from app.roles.models import Role
from app.users.models import User
from app.users.utils import is_user_already_exist, find_user_by_name, create_user_session_connect, \
    create_seller_session_connect
from app.shared.serializers import get_error_response

users = Blueprint('users', __name__, url_prefix='/users')


@users.route('/register', methods=['POST'])
def register():
    username = request.json.get('username', None)
    email = request.json.get('email', None)

    message = is_user_already_exist(username, email)

    if type(message) == str:
        return get_error_response(message, 400)

    status = request.json.get('is_seller', None)
    first_name = request.json.get('first_name', None)
    last_name = request.json.get('last_name', None)
    password = request.json.get('password', None)
    role = Role.query.filter(Role.name == 'ROLE_USER').first()

    if status:
        role = Role.query.filter(Role.name == 'ROLE_SELLER').first()

    db.session.add(
        User(first_name=first_name,
             last_name=last_name,
             username=username,
             password=bcrypt.generate_password_hash(password).decode('utf-8'),
             roles=[role],
             email=email))
    db.session.commit()
    user = find_user_by_name(username)

    if user:
        access_token = create_access_token(identity=user.id, fresh=True)
        refresh_token = create_refresh_token(user.id)

        return jsonify({
            'success': True,
            'user': {
                'access_token': access_token,
                'refresh_token': refresh_token,
                'roles': [role.name for role in user.roles]
            }
        }), 201

    return get_error_response(
        'REGISTERING FAILED SORRY WE MAY BE HAVE A PROBLEM')


@users.route('/login-client', methods=['POST'])
def login_as_client():
    email = request.json.get('email', None)
    password = request.json.get('password', None)

    if email is None:
        return jsonify({'message': 'YOUR EMAIL IS MISSING'}), 400
    if password is None:
        return jsonify({'message': 'YOUR PASSWORD IS MISSING'}), 400

    msg = is_user_already_exist(email=email)

    if type(msg) == str:
        return create_user_session_connect(email, password)
    return get_error_response('INVALID CREDENTIAL', 404)


@users.route('/login-seller', methods=['POST'])
def login_as_seller():
    email = request.json.get('email', None)
    password = request.json.get('password', None)

    if email is None:
        return jsonify({'message': 'YOUR EMAIL IS MISSING'}), 400
    if password is None:
        return jsonify({'message': 'YOUR PASSWORD IS MISSING'}), 400

    msg = is_user_already_exist(email=email)

    if type(msg) == str:
        return create_seller_session_connect(email, password)
    return get_error_response('INVALID CREDENTIAL', 404)
