from flask import jsonify
from flask_jwt_extended import create_access_token, create_refresh_token
from app.shared.serializers import get_error_response
from app.users.models import User


def find_user_by_name(username):
    return User.query.filter(User.username == username).first()


def find_user_by_id(user_id):
    return User.query.filter(User.id == user_id).first()


def find_user_by_email(user_email):
    return User.query.filter(User.email == user_email).first()


def is_user_already_exist(username=None, email=None):
    if username:
        if find_user_by_name(username):
            return 'THIS USERNAME ALREADY EXITS'
    if email:
        if find_user_by_email(email):
            return 'THIS EMAIL ALREADY EXITS'

    return None


def create_user_session_connect(email, password):
    user = find_user_by_email(email)
    if not user.is_seller():
        if user.is_password_valid(str(password)):

            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)

            return jsonify({
                'success': True,
                'user': {
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'roles': [role.name for role in user.roles]
                }
            }), 200
        else:
            return get_error_response('YOUR CREDENTIAL DOESN\'T MATCH RECORD', 401)
    return get_error_response('UNAUTHORIZED', 401)


def create_seller_session_connect(email, password):
    user = find_user_by_email(email)
    if user.is_seller():
        if user.is_password_valid(str(password)):

            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)

            return jsonify({
                'success': True,
                'user': {
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'roles': [role.name for role in user.roles]
                }
            }), 200
        else:
            return get_error_response('YOUR CREDENTIAL DOESN\'T MATCH RECORD', 401)

    return get_error_response('UNAUTHORIZED', 401)
