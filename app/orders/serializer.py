from app.shared.serializers import PageSerializer


class OrderListSerializer(PageSerializer):
    resource_name = 'orders'

