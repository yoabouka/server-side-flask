from flask import Blueprint

main = Blueprint('main', __name__, url_prefix='/main')


@main.route('/home')
def index():
    return '<h1> Welcome autopart deal </h1>'
