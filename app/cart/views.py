from autopartdeals import create_app
from flask_sqlalchemy import SQLAlchemy

application = create_app()
dbs = SQLAlchemy(application)

application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///C:\\users\\hp\\PFE\\'
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


class Cart:

    def __init__(self, items):
        self.items = items
