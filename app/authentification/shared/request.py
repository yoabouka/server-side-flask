from app.authentification.shared.validation import ValidationService, RequiredRule, EmailRule, MinLengthRule
from app.authentification.domain.entities import User


# request definition
class ValidRequest(object):
    def __init__(self, adict=None):
        self.attributes = adict

    @classmethod
    def from_dict(cls, adict):
        return cls(adict)

    def __getattr__(self, value):
        if value in self.attributes:
            return self.attributes[value]
        raise AttributeError(
            f'the {value} attribute does not exist on the object')

    def __nonzero__(self):
        return True


class InvalidRequest(object):
    def __init__(self, errors=[]):
        self.errors = errors

    def has_errors(self):
        return len(self.errors) > 0

    def add_error(self, parameter: str, message: str):
        self.errors.append({'parameter': parameter, 'message': message})

    @classmethod
    def from_dict(cls, errors):
        cls_errors = []
        for parameter, messages in errors.items():
            cls_errors.append({'parameter': parameter, 'message': messages})
        return cls(cls_errors)

    def __nonzero__(self):
        return False

    __bool__ = __nonzero__


class RegisterUserRequest(ValidRequest):
    @classmethod
    def build_from_dict(cls, adict):
        invalid_request = InvalidRequest()
        if not isinstance(adict, dict):  # demander pourquoi cette condition
            invalid_request.add_error('NoField', 'No field specified')
            return invalid_request
        # error losque le dictionaire est vide a regler
        validation_service = ValidationService(adict)
        validation_service.add_rules({
            'firstName': [RequiredRule.build()],
            'lastName': [RequiredRule.build()],
            'username': [RequiredRule.build()],
            'email': [RequiredRule.build(), EmailRule()],
            'password': [RequiredRule.build(),
                         MinLengthRule.build(6)]
        })
        invalid_request = InvalidRequest.from_dict(
            validation_service.validate())

        if invalid_request.has_errors():
            return invalid_request

        return RegisterUserRequest(adict)


class ReauthenticateUserRequest(ValidRequest):
    @classmethod
    def build_from_dict(cls, adict):
        invalid_request = InvalidRequest()
        if not isinstance(adict, dict):
            invalid_request.add_error("NoField", "No field specified")
            return invalid_request

        validation_service = ValidationService(adict)
        validation_service.add_rules({
            'user': [RequiredRule.build()],
        })
        invalid_request = InvalidRequest.from_dict(
            validation_service.validate())

        if invalid_request.has_errors():
            return invalid_request

        return ReauthenticateUserRequest(adict)


class LogInUserRequest(ValidRequest):
    @classmethod
    def build_from_dict(cls, adict):
        invalid_request = InvalidRequest()
        if not isinstance(adict, dict):
            invalid_request.add_error("NoField", "No field specified")
            return invalid_request

        validation_service = ValidationService(adict)
        validation_service.add_rules({
            'email': [RequiredRule.build(), EmailRule()],
            'password': [RequiredRule.build()]
        })
        invalid_request = InvalidRequest.from_dict(
            validation_service.validate())

        if invalid_request.has_errors():
            return invalid_request

        return LogInUserRequest(adict)


class LogoutUserRequest(ValidRequest):
    pass


class GetInfosUserRequest(ValidRequest):
    @classmethod
    def build_from_dict(cls, adict):
        invalid_request = InvalidRequest()
        if not isinstance(adict, dict):
            invalid_request.add_error('email', 'Not correct email')
            return invalid_request
        validation_service = ValidationService(adict)
        validation_service.add_rules(
            {'email': [RequiredRule.build(), EmailRule()]})
        invalid_request = InvalidRequest.from_dict(
            validation_service.validate())

        if invalid_request.has_errors():
            return invalid_request

        return GetInfosUserRequest(adict)


class EditInfosUserRequest(ValidRequest):
    pass


class ForgotPasswordRequest(ValidRequest):
    pass
