from app.adresses.models import Address
from app.categories.models import Category
from app.comments.models import Comment
from app.factory import db, create_app, bcrypt
from app.file_Uploads.models import ProductImage
from app.products.models import Product, PartData
from app.roles.models import Role
from app.users.models import User

create_app().app_context().push()


user3 = User.query.filter(User.id == 3).first()

for address in user3.addresses:
    print(address.get_summary())