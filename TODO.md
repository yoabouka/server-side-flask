# Roadmap

## Order management

- Saving orders sent by external client
- Cancel orders sent by external client
- Get all orders of an external client
- Get all orders of an external seller
- Get order details from a client
- Get order details from a seller
- Handle orders (seller level task)

## Product management

- Listing products
- Retrieving a single product details
- Editing a product
- Adding a new product
- Deleting a product
- Listing products of a specific seller
- Retrieving products by category
- Search products

## Category management

- Listing categories
- Add a new category
